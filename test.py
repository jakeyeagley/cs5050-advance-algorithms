import sys

price = [1,5,8,9]
val = [0,4,8,2]
weight = [1,2,3,4]
v = [1,2,3,4,5,6]
v1 = [1,0,2,9,3,8,4,7,6,5]

dictionary = {"egibb","bin","jim","hello","what","att","he"}

def rob(n,L):
    if L <= 0 or n == 0:
        return 0
    if weight[n-1] > L:
        return rob(n-1,L)
    else:
        return max(val[n-1] + rob(n-1,L-weight[n-1]),rob(n-1,L))

def bv(L):
    if L == 0:
        return 0
    else:
        value = -sys.maxsize-1
        for i in range(1,L):
            value = max(value, price[i] + bv(L - i - 1))
        return value
    
def pick(n,v):
    print('v: ',v)
    if n == 0:
        return 0
    else:
        remove = min(v[0],v[n-1])
            
        return min(v[n-1] + pick(n-1),v[0] + pick(n-1))

def splitWord(phrase):
    if len(phrase) == 0:
        return True
    for i in range (0,len(phrase) + 1):
        if phrase[:i] in dictionary and splitWord(phrase[i:]):
            return True
    return False

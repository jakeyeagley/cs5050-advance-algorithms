#!/user/bin/python

import numpy as np
import random
import math
from matplotlib import pyplot as plt
from time import time

def polyMulti(A,B):
    AB = []
    for i in range(0,len(A) + len(B) - 1):
        AB.append(0)
    
    for i in range(0,len(A)):
        for j in range(0,len(B)):
            AB[i + j] = A[i] * B[j] + AB[i + j]

    return AB

def polySolver(A,B):
    if len(A) <= 2 and len(B) <= 2:
        return polyMulti(A,B)
    
    Pl = A[:len(A)//2]
    Pr = A[len(A)//2:]

    Ql = B[:len(B)//2]
    Qr = B[len(B)//2:]

    # add the indexs
    PrPLUSPl = [x + y for x, y in zip(Pl, Pr)]
    QrPLUSQl = [x + y for x, y in zip(Ql, Qr)]
    leftSide = polySolver(PrPLUSPl,QrPLUSQl)
    PlQl = polySolver(Pl,Ql)
    PrQr = polySolver(Pr,Qr)

    tempMiddle = [x - y for x, y in zip(leftSide, PrQr)]
    middle = [x - y for x, y in zip(tempMiddle, PlQl)]

    size = (2 *len(A)) - 1
    
    leftCount = len(PlQl)//2
    rightCount = len(PrQr)//2 + 1
    rightCount = size - rightCount
    midCount = size//2

    ans = []
    m = 0
    r = 0
    for l in range(0,size):
        # add to the left side from PlQl
        if l <= leftCount:
            ans.append(PlQl[l])
        # add the index where middle and PlQl over lap
        elif l > leftCount and l < midCount:
            ans.append(PlQl[l] + middle[m])
            m = m + 1
        # add the middle 
        elif l == midCount:
            ans.append(middle[m])
            m = m + 1
        # add the index where middle and PrQr over lap
        elif l > midCount and l < rightCount:
            ans.append(middle[m] + PrQr[r])
            m = m + 1
            r = r + 1
        # add to the right side from PrQr
        elif l >= rightCount:
            ans.append(PrQr[r])
            r = r + 1
    return ans

    
def testRun():
    sizeA = int(input('sizeA: '))

    A = []
    for i in range(0,sizeA):
        temp = int(input(': '))
        A.append(temp)

    sizeB = int(input('sizeB: '))
    B = []
    for i in range(0,sizeB):
        temp = int(input(': '))
        B.append(temp)
        
    print(polySolver(A,B))
    print(polyMulti(A,B))

def run(testLen):
    timesOld = []
    timesNew = []
    
    ns = []
    
    n = 33
    for i in range(0, testLen):
        # set n (starts at 32 and doubles ever iteration

        # loop to set polynomial arrays
        A = []
        B = []
        for j in range(1,n):
            A.append(random.uniform(-1.0,1.0))
            B.append(random.uniform(-1.0,1.0))
        
		# loop to get average of time runs
        timeAveNew = []
        timeAveOld = []
        for j in range(0,10):
            start = time()
            polySolver(A,B)
            end = time()
            timeAveNew.append(end-start)
            
            start = time()
            polyMulti(A,B)
            end = time()
            timeAveOld.append(end-start)
            
        print(n - 1,': old:',sum(timeAveOld))
        print(n - 1,': new:',sum(timeAveNew))

        filename = open('times.txt', 'a')
        filename.write('Old: ')
        filename.write(str(n - 1))
        filename.write(': ')
        mark = sum(timeAveOld)
        filename.write(str(mark))
        filename.write('\n')
        
        filename.write('New: ')
        filename.write(str(n - 1))
        filename.write(': ')
        mark = sum(timeAveNew)
        filename.write(str(mark))
        filename.write('\n')
        filename.close()

        timesOld.append(sum(timeAveOld))
        timesNew.append(sum(timeAveNew))
        ns.append(n)
        n = n * 2
        if n % 2 == 0:
            n = n - 1

    logTimesOld = []
    logTImesNew = []
    logNs = []
    
    logTimesOld = [math.log(x,10) for x in timesOld]
    logTimesNew = [math.log(x,10) for x in timesNew]
    logNs = [math.log(x,10) for x in ns]

    fit1 = np.polyfit(logNs,logTimesOld,1)
    fit_fn1 = np.poly1d(fit1)

    fit2 = np.polyfit(logNs,logTimesNew,1)
    fit_fn2 = np.poly1d(fit2)
    
    plt.plot(logNs, logTimesOld, 'yo', logNs, fit_fn1(logNs), label='old polynomial algorithm', color='Red')
    plt.plot(logNs, logTimesNew, 'yo', logNs, fit_fn2(logNs), label='new polynomial algorithm', color='Blue')
    plt.legend(loc='upper right')
    plt.ylabel('times (ms) ')
    plt.xlabel('n')
    plt.show()
    
if __name__ == '__main__':
    runLen = int(input("run time: "))
    run(runLen)
	#testRun() 

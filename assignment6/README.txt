The value of where the naive polynomial algorithm and new algorithm cross is between n = 1024 and n = 2048. The significance of this cross is this point is where the new algorithm is quicker and solves larger polynomials faster than the naive polynomial.

If we ignore data points where n < 1024 we see that the new polynomial algorithm is faster and the difference between the old algorithm and new algorithm gets larger as n gets larger. 

#!/user/bin/python

import random
import math
from matplotlib import pyplot as plt
from time import time

def polySolver(A,B):
    AB = []
    for i in range(0,len(A) + len(B) - 1):
        AB.append(0)
    
    for i in range(0,len(A)):
        for j in range(0,len(B)):
            AB[i + j] = A[i] * B[j] + AB[i + j]

    # print ('AB: ', AB)

def run(testLen):
    times = []
    ns = []
    
    n = 32
    for i in range(0, testLen):
        # set n (starts at 32 and doubles ever iteration

        # loop to set polynomial arrays
        A = []
        B = []
        for j in range(1,n):
            A.append(random.uniform(-1.0,1.0))
            B.append(random.uniform(-1.0,1.0))
            
        # loop to get average of time runs
        timeAve = []
        for j in range(0,10):
            start = time()
            polySolver(A,B)
            end = time()
            timeAve.append(end-start)
        print(n,': ',sum(timeAve))
        times.append(sum(timeAve))

        ns.append(n)
        n = n * 2

    logTimes = []
    logNs = []
    
    logTimes = [math.log(x,10) for x in times]
    logNs = [math.log(x,10) for x in ns]

    plt.plot(logNs, logTimes, label='log v log', color='blue')
    plt.legend(loc='upper right')
    plt.ylabel('times (ms) ')
    plt.xlabel('n')
    plt.show()
    
if __name__ == '__main__':
    runLen = int(input("run time: "))
    run(runLen)

    
    

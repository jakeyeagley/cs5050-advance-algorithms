cache = {}

def win(A,B,C):

    print "A: ",A
    print "B: ",B
    print "C: ",C

    # win
    if (A == 0 and B == 0 and C == 0):
        return False
    elif (A == 1 and B == 1 and C == 1):
        return False

    # lose
    elif (A == 1 and B == 0 and C == 0):
        return True
    elif (A == 0 and B == 1 and C == 0):
        return True
    elif (A == 0 and B == 0 and C == 1):
        return True
    

    elif (A,B,C) in cache.keys():
        return cache[(A,B,C)]

    else:
        for i in range(1,A):
            cache[(A,B,C)] = not win(A-i,B,C)
            return cache[(A,B,C)]
        for i in range(1,B):
            cache[(A,B,C)] = not win(A,B-i,C)
            return cache[(A,B,C)]
        for i in range(1,C):
            cache[(A,B,C)] = not win(A,B,C-i)
            return cache[(A,B,C)]

A = input("A: ")
B = input("B: ")
C = input("C: ")

print win(A,B,C)
#!/user/bin/python

from xlsxwriter import Workbook
import random
import math
from time import time
from matplotlib import pyplot as plt

def naive(s,m):
   indexes = []
   for i in range(0,len(s)):
       found = True
       for j in range(0,len(m)):
           if i + j >= len(s):
               return indexes
           found = found and s[i + j] == m[j]
           if not found: 
               break
       if found:
           indexes.append(i)

   return indexes

NO_OF_CHARS = 256

def badCharHeuristic(txt, size): 
	badChar = [-1]*NO_OF_CHARS 

	for i in range(size): 
		badChar[ord(txt[i])] = i; 

	return badChar 

def BM(txt, pat):
   indexes = []
   patSize = len(pat)
   txtSize = len(txt)
   badChar = badCharHeuristic(pat, patSize)
   print 'badChar:', badChar

   s = 0

   # s is txt index
   # j is pat index
   while(s <= txtSize-patSize):
      j = patSize-1
      while j>=0 and pat[j] == txt[s+j]:
         j -= 1
      if j<0:
         indexes.append(s)
         s += (patSize-badChar[ord(txt[s+patSize])] if s+patSize<txtSize else 1)
      else:
            s += max(1, j-badChar[ord(txt[s+j])])
   return indexes

def KMP(txt, pat):
   indexes = []

   patSize = len(pat)
   txtSize = len(txt)

   # create lps[] that will hold the longest prefix suffix
   # values for pattern

   lps = [0]*patSize
   j = 0
   # Preprocess the pattern (calculate lps[] array)

   computeLPSArray(pat, patSize, lps)

   i = 0 # index for txt[]
   while i < txtSize:
      if pat[j] == txt[i]:
         i += 1
         j += 1

      if j == patSize:
         indexes.append(i - j)
         j = lps[j-1]

      # mismatch after j matches
      elif i < txtSize and pat[j] != txt[i]:
         # Do not match lps[0..lps[j-1]] characters,
         # they will match anyway
         if j != 0:
            j = lps[j-1]
         else:
            i += 1
   return indexes

def computeLPSArray(pat, patSize, lps):
   len = 0 # length of the previous longest prefix suffix

   lps[0] = 0
   i = 1
   # the loop calculates lps[i] for i = 1 to patSize-1
   while i < patSize:
      if pat[i]== pat[len]:
         len += 1
         lps[i] = len
         i += 1
      else:
         # This is tricky. Consider the example.
         # AAACAAAA and i = 7. The idea is similar
         # to search step.
         if len != 0:
            len = lps[len-1]
            # Also, note that we do not increment i here
         else:
            lps[i] = 0
            i += 1

def string_generator(p, size):
   str = ""
   state = True
   for i in range(0,size):
      if random.uniform(0,1) >= p:
         state = not state 
      if state:
         str += '0'
      else:
         str += '1'

   return str
 
if __name__ == '__main__':
   s = "this is suppose to be a really long sentence"
   print('test string: ', s)
   print("test pattern: 'is'")
   print('Naive: ',naive(s, 'is'))
   print('BM: ',BM(s,'is'))
   print('KMP: ',KMP(s,'is'))

   s = "abcdefghijklmnopqrstuvwxyz abcdefghijklmnopqrstuvwxyz abcdefghijklmnopqrstuvwxyz "
   print('test string: ', s)
   print("test pattern: 'xyz'")
   print('Naive: ',naive(s, 'xyz'))
   print('BM: ',BM(s,'xyz'))
   print('KMP: ',KMP(s,'xyz'))
   
   s = "a"
   print('test string: ', s)
   print("test pattern: 'b'")
   print('Naive: ',naive(s, 'b'))
   print('BM: ',BM(s,'b'))
   print('KMP: ',KMP(s,'b'))

   binaryStr1 = string_generator(.5,1000000)
   binaryStr2 = string_generator(.99,1000000)
   shakespeare = open('shakespeare.txt').read()
   DNA = open('DNA.txt').read()

   print('strings generated')
   
   size = int(input('run size: '))

   timesDN = []
   timesDB = []
   timesDK = []
   for i in range(1,size + 1):
      print('checking: ',DNA[-(i**2):])
      start = time()
      naive(DNA,DNA[-(i**2):])
      end = time()
      timesDN.append(end-start)

      print('appended: ',end-start, 'to Naive for DNA')

      start = time()
      BM(DNA,DNA[-(i**2):])
      end = time()
      timesDB.append(end-start)

      print('appended: ',end-start, 'to BM for DNA')

      start = time()
      KMP(DNA,DNA[-(i**2):])
      end = time()
      timesDK.append(end-start)
      
      print('appended: ',end-start, 'to KMP for DNA')
 
   workbook = Workbook('times2.xlsx')
   Report_Sheet = workbook.add_worksheet()
   Report_Sheet.write(0,0,'DNA naive')
   Report_Sheet.write_column(1,0,timesDN)  
   Report_Sheet.write(0,1,'DNA BM')
   Report_Sheet.write_column(1,1,timesDB)  
   Report_Sheet.write(0,2,'DNA KMP')
   Report_Sheet.write_column(1,2,timesDK)  


   timesSN = []
   timesSB = []
   timesSK = []
   for i in range(1,size + 1):
      print('checking: ',shakespeare[-(i**2):])
      start = time()
      naive(shakespeare,shakespeare[-(i**2):])
      end = time()
      timesSN.append(end-start)

      print('appended: ',end-start, 'to Naive for shakespeare')

      start = time()
      BM(shakespeare,shakespeare[-(i**2):])
      end = time()
      timesSB.append(end-start)

      print('appended: ',end-start, 'to BM for shakespeare')

      start = time()
      KMP(shakespeare,shakespeare[-(i**2):])
      end = time()
      timesSK.append(end-start)
      
      print('appended: ',end-start, 'to KMP for shakespeare')
   
   Report_Sheet.write(0,3,'shakespeare naive')
   Report_Sheet.write_column(1,3,timesSN)  
   Report_Sheet.write(0,4,'shakespeare BM')
   Report_Sheet.write_column(1,4,timesSB)  
   Report_Sheet.write(0,5,'shakespeare KMP')
   Report_Sheet.write_column(1,5,timesSK) 

   times1N = []
   times1B = []
   times1K = []
   for i in range(1,size + 1):
      print('checking: ',binaryStr1[-(i**2):])
      start = time()
      naive(binaryStr1,binaryStr1[-(i**2):])
      end = time()
      times1N.append(end-start)
      
      print('appended: ',end-start, 'to Naive for first binary string')

      start = time()
      BM(binaryStr1,binaryStr1[-(i**2):])
      end = time()
      times1B.append(end-start)

      print('appended: ',end-start, 'to BM for first binary string')

      start = time()
      KMP(binaryStr1,binaryStr1[-(i**2):])
      end = time()
      times1K.append(end-start)
      
      print('appended: ',end-start, 'to KMP for first binary string')

   Report_Sheet.write(0,6,'binary1 naive')
   Report_Sheet.write_column(1,6,times1N)  
   Report_Sheet.write(0,7,'binary1 BM')
   Report_Sheet.write_column(1,7,times1B)  
   Report_Sheet.write(0,8,'binary1 KMP')
   Report_Sheet.write_column(1,8,times1K) 

   times2N = []
   times2B = []
   times2K = []
   for i in range(1,size + 1):
      print('checking: ',binaryStr2[-(i**2):])
      start = time()
      naive(binaryStr2,binaryStr2[-(i**2):])
      end = time()
      times2N.append(end-start)

      print('appended: ',end-start, 'to Naive for second binary string')

      start = time()
      BM(binaryStr2,binaryStr2[-(i**2):])
      end = time()
      times2B.append(end-start)

      print('appended: ',end-start, 'to BM for second binary string')

      start = time()
      KMP(binaryStr2,binaryStr2[-(i**2):])
      end = time()
      times2K.append(end-start)
      
      print('appended: ',end-start, 'to KMP for second binary string')


   Report_Sheet.write(0,9,'binary2 naive')
   Report_Sheet.write_column(1,9,times2N)  
   Report_Sheet.write(0,10,'binary2 BM')
   Report_Sheet.write_column(1,10,times2B)  
   Report_Sheet.write(0,11,'binary2 KMP')
   Report_Sheet.write_column(1,11,times2K)
 
   workbook.close()

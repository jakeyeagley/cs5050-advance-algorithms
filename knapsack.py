#!/usr/bin/python
from random import randrange as rand
from time import time
from matplotlib import pyplot as plt
import math

def knapRecursive(n, l1, l2):
    if l1 == 0 and l2 == 0:
        return True
    elif n == 0:
        return False
    elif l1 < 0 or l2 < 0:
        return False
    else:
        return knapRecursive(n-1, l1, l2) or knapRecursive(n-1, l1-s[n - 1], l2) or knapRecursive(n-1, l1, l2-s[n - 1])
    

def knapMemo(n, l1, l2):
    if l1 == 0 and l2 == 0:
        return True
    elif n == 0:
        return False
    elif l1 < 0 or l2 < 0:
        return False
    elif (n,l1,l2) in cache.keys():
        return cache[(n,l1,l2)]
    else:
        cache[(n,l1,l2)] = knapMemo(n-1, l1, l2) or knapMemo(n-1, l1-s[n - 1], l2) or knapMemo(n-1, l1, l2-s[n - 1])
        return cache[(n,l1,l2)]      

def problemGenerator(n,m):
    problemSet = []
    for i in range(0,n):
        problemSet.append(rand(1,m-1))

    return problemSet

def runMemoizing(n,m,l1,l2):
    timesAve = []
    for j in range(1, n + 1):
        times = []
        for i in range(0,10):
            
            global s
            s = problemGenerator(j * 10,m)
            global cache
            cache = {}
            
            start = time()
            knapMemo(len(s),l1,l2)
            end = time()

            times.append(end - start)
        print('time mark, memoizing: ', sum(times))
        timesAve.append(sum(times))
    return timesAve

def runRecusion(n,m,l1,l2):
    timesAve = []
    for j in range(1, n + 1):
        times = []
        for i in range(0 ,10):
            global s
            s = problemGenerator(j * 10,m)
            start = time()
            knapRecursive(len(s),l1,l2)
            end = time()

            times.append(end - start)
        print('time mark, recursion: ', sum(times))
        timesAve.append(sum(times))
    return timesAve

def getObjectCount(n):
    counts = []
    for i in range(1,n + 1):
        counts.append(i * 10)

    return counts

if __name__ == '__main__':
    print('testing functions with s[10,2,3], l1 = 10, and l2 = 5. This should be true')

    s = [10,2,3]
    l1 = 10
    l2 = 5
    print('recursion: ', knapRecursive(len(s),l1,l2))

    cache = {}
    print('memoizing: ', knapMemo(len(s),l1,l2))

    print('testing functions with s[10,20], l1 = 10, and l2 = 5. This should be false')
    s = [10,20]
    l1 = 10
    l2 = 5
    print('recursion: ', knapRecursive(len(s),l1,l2))

    
    cache = {}
    print('memoizing: ', knapMemo(len(s),l1,l2))

    print('testing functions with s[1,2,3,4], l1 = 6, and l2 = 4. This should be true')
    s = [1,2,3,4]
    l1 = 6
    l2 = 4
    print('recursion: ', knapRecursive(len(s),l1,l2))

    cache = {}
    print('memoizing: ', knapMemo(len(s),l1,l2))

    print('testing functions with s[1,2,3,4,5,6], l1 = 11, and l2 = 10. This should be true')
    s = [1,2,3,4,5,6]
    l1 = 11
    l2 = 10
    print('recursion: ', knapRecursive(len(s),l1,l2))

    cache = {}
    print('memoizing: ', knapMemo(len(s),l1,l2))

    print('testing functions with s[1,1,1,1,1,1,1,1,1,1,2,2,2,2,2], l1 = 10, and l2 = 10. This should be true')
    s = [1,1,1,1,1,1,1,1,1,1,2,2,2,2,2]
    l1 = 10
    l2 = 10
    print('recursion: ', knapRecursive(len(s),l1,l2))

    cache = {}
    print('memoizing: ', knapMemo(len(s),l1,l2))
    while True:
        n = int(input('set n: '))
        m = int(input('set m: '))

        l1 = int(input('set l1: '))
        l2 = int(input('set l2: '))


        memo = runMemoizing(n,m,l1,l2)
        memoLog = []
        
        for i in range(len(memo)):
            memoLog.append(math.log(memo[i]))

        rec = runRecusion(n,m,l1,l2)
        recLog = []

        for j in range(len(rec)):
            recLog.append(math.log(rec[j]))

        oc = getObjectCount(n)

        plt.plot(oc,memo, label="memo", color="blue")
        plt.plot(oc,rec, label="rec", color="red")
        plt.legend(loc='upper right')
        plt.xlabel('object count')
        plt.ylabel('time(s)')
        plt.show()

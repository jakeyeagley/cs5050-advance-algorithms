cache = {}

def win(n):
    if n == 0:
        return False

    elif n == 1:
        return True

    elif n in cache.keys():
        return cache[n]

    else:
        cache[n] = not(win(n-1) and win(n-2))
        return cache[n]

for i in range(0,5000):
    print i, " ", win(i)
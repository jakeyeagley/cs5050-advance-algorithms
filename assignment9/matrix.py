import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt

y_labels = ['bcl380',
'pbk411',
'pbl395',
'pbm436',
'pbn423',
'pka379',
'pma343',
'xqf131',
'xqg237',
'xql662'
]

x_labels = [
'simple',
'branch/bound',
'greedy',
'Reverse sub tour',
'swap cities',
]

if __name__ == '__main__':
    data = pd.read_csv('data.csv')

    fig, ax = plt.subplots()
    heatmap = ax.pcolor(data, cmap=plt.cm.Blues)

    plt.yticks( [0.5,1.5,2.5,3.5,4.5,5.5,6.5,7.5,8.5,9.5],y_labels,rotation=17)
    plt.xticks( [0.5,1.5,2.5,3.5,4.5],x_labels,rotation=17)

    for x in range(0,5):
        for y in range(len(data['simple'])):
            plt.text(x + 0.5, y + 0.5, round(data[x_labels[x]][y],2),horizontalalignment='center',verticalalignment='center')

    ax.axis('tight')
    plt.colorbar(heatmap)

    plt.show()


# file
#

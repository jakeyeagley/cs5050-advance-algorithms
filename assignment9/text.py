s = [2,3,5]
value = [2,33,555]
cache = [None] * (len(s) + 1)

def knap(N,K):
    global s
    global value
    global cache

    temp = [0] * (K+1)
    for i in range(0,N+1):
        cache[i] = temp
    print 'cache here', cache
    for i in range(0,N+1):
        cache[i][0] = 0
    for j in range(1,K+1):
        cache[0][j] = 0
    print 'N:',N
    print 'K:',K
    for i in range(1,N+1):
        print 'i:',i
        for j in range(1,K+1):
            print 'j:',j
            newSackSize = j - s[i-1]
            print 'newSackSize:', newSackSize
            if newSackSize < 0:
                cache[i][j] = cache[i-1][j]
            else:
                print 'value[i-1]:', value[i-1]
                cache[i][j] = max(cache[i-1][j], value[i-1] + cache[i][newSackSize])

    return cache

def trace(i,k,cache):
    global s

    print 'cache:', cache
    print 'i:', i
    print 'k:', k
    if i ==0 or k == 0:
        return []

    newJ = k - s[i-1]
    print 's[i-1]: ', s[i-1]
    print 'newJ: ', newJ
    if newJ < 0:
        return trace(i-1,k,cache)
    print 'here:', i
    return [i] + trace(i - 1,newJ,cache)
if __name__ == '__main__':
    k = 5
    print 'knap:' ,knap(len(s),k)
    print trace(len(s) - 1,k,cache)
 

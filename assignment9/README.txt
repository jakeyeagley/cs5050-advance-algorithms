In this assignment it looks like the greedy algorithm determined the shortest paths in the given amount of time. Algorithms 4 and 5 (the mutation or guess and check algorithms) did the worst. It is interesting to note that the branch and bound and the simple algorithms look very similiar, as do the swap two cities and reverse sub tour algorithms. 

I added into algorithms 4 and 5 a time to re-randomize the path being mutated to see if that would improve the outcome. It did little to improve the algorithms.

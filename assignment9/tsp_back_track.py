import sys
import time
import random
import math

locations = {}
end_time = 8

def readLocations(fileName):
    f = open(fileName,"r")
    for line in f:
        line = line.rstrip()
        line = line.split(' ')
        locations[str(line[0])] = [int(line[1]),int(line[2])]

def distance(city1,city2):
    p0 = locations[str(city1)]
    p1 = locations[str(city2)]
    return math.sqrt((p0[0] - p1[0])**2 + (p0[1] - p1[1])**2)

# cc = current city
# rc = remaining cities
# dsf = distance so far
def tsp_simple(cc,rc,dsf,N):
    t = time.clock()
    b = sys.maxint
    return _tsp_simple(cc,rc,dsf,N - 1,b,t)

def _tsp_simple(cc,rc,dsf,N,bsf,t):
    if rc == []:
        return dsf + distance(1,cc)
    best = sys.maxint
    if t > end_time:
        return bsf
    for i in range(0,len(rc)):
        best = min(_tsp_simple(rc[i],rc[0:i] + rc[i+1:N], dsf + distance(cc,rc[i]), N,bsf, time.clock()),best)
        bsf = best
    return bsf

# cc = current city
# rc = remaining cities
# dsf = distance so far
# bsf = best so far
def tsp_branch_bound(cc,rc,dsf,N):
    bsf = sys.maxint
    t = time.clock()
    return _tsp_branch_bound(cc,rc,dsf,N - 1,bsf,t)

def _tsp_branch_bound(cc,rc,dsf,N,bsf,t):
    if rc == []:
        return dsf + distance(1,cc)
    if dsf > bsf:
        return bsf
    best = sys.maxint
    for i in range(0,len(rc)):
        if t > end_time:
            return bsf
        best = min(_tsp_branch_bound(rc[i],rc[0:i] + rc[i+1:N], dsf + distance(cc,rc[i]), N, bsf, time.clock()),best)
        bsf = best
    return best

# cc = current city
# rc = remaining cities
# dsf = distance so far
# bsf = best so far
def sort_current_city(cc, rc):
    locs = {}
    for i in range(0,len(rc)):
        locs[str(rc[i])] = distance(cc,rc[i])

    loc_dist = {}
    loc_dist = sorted(locs.iteritems(), key=lambda (k,v): (v,k))

    ret = []
    for i in range(0,len(loc_dist)):
        if int(loc_dist[i][1]) != 0:
            ret.append(int(loc_dist[i][0]))
    return ret

def tsp_greedy(cc,rc,dsf,N):
    bsf = sys.maxint
    t = time.clock()
    return _tsp_greedy(cc,rc,dsf,N - 1,bsf,t)

def _tsp_greedy(cc,rc,dsf,N,bsf,t):
    rc = sort_current_city(cc,rc)
    if rc == []:
        return dsf + distance(1,cc)
    if dsf > bsf:
        return sys.maxint
    best = sys.maxint
    for i in range(0,len(rc)):
        if t > end_time:
            return bsf
        best = min(_tsp_greedy(rc[i],rc[0:i] + rc[i+1:N], dsf + distance(cc,rc[i]), N, bsf, time.clock()),best)
        bsf = best
    return best

def randomPath():
    path = list(range(1,len(locations) + 1))
    path = random.sample(path,len(locations))
    get_distance_so_far(path)
    return path

def get_distance_so_far(path):
    if len(path) <= 1:
        return 0
    dist = []
    for i in range(0, len(path) - 1):
        dist.append(distance(path[i],path[i+1]))

    return sum(dist)

def mutate_swap_two_cities():
    t = time.clock()
    path = randomPath()
    count = 0
    bdsf = get_distance_so_far(path)
    global end_time
    while time.clock() - t <= end_time:
        count = count + 1
        if count % 100 == 0:
            path = randomPath()
        city1 = random.randint(0,len(path) - 1)
        city2 = random.randint(0,len(path) - 1)
        pathBefore = path
        path[city1], path[city2] = path[city2], path[city1]
        after = get_distance_so_far(path)
        if round(after,3) <= round(bdsf,3):
            bdsf = after

    return bdsf

def reverse_tour(path):
    start = random.randint(1,(len(path) - 1) / 2)
    end = random.randint((len(path)) / 2, len(path))
    subPath = path[start:end]
    reverse = subPath[::-1]
    path[start:end] = reverse
    return path

def mutate_reverse_sub_tour():
    t = time.clock()
    path = randomPath()
    count = 0
    bdsf = get_distance_so_far(path)
    global end_time
    while time.clock() - t <= end_time:
        count = count + 1
        if count % 100 == 0:
            path = randomPath()
        city1 = random.randint(0,len(path) - 1)
        city2 = random.randint(0,len(path) - 1)
        reversedPath = reverse_tour(path)
        reversedPathDistance = get_distance_so_far(reversedPath)
        if round(reversedPathDistance,3) <= round(bdsf,3): bdsf = reversedPathDistance

    return bdsf

if __name__ == '__main__':
    locations = {'1': [1,0],
                 '2': [2,0],
                 '3': [2,1],
                 '4': [2,2],
                 '5': [3,2],
                 '6': [2,3],
                 '7': [4,3],
                 '8': [2,5],
                 '9': [5,3],
                }
    rc = range(1,len(locations) + 1)
    print 'demo'
    print locations

    print 'simple'
    print tsp_simple(1,rc,0,len(locations))

    print 'greedy'
    print tsp_greedy(1,rc,0,len(locations))

    print 'branch and bound'
    print tsp_branch_bound(1,rc,0,len(locations))

    print 'swap cities'
    print mutate_swap_two_cities()

    print 'reverse sub tour'
    print mutate_reverse_sub_tour()

    if len(sys.argv) != 1:
        readLocations(sys.argv[2])
        rc = range(1,len(locations) + 1)
        if sys.argv[1] == '-s':
            print tsp_simple(1,rc,0,len(locations))
            print 'file: ', sys.argv[2]
        if sys.argv[1] == '-g':
            print tsp_greedy(1,rc,0,len(locations))
            print 'file: ', sys.argv[2]
        if sys.argv[1] == '-b':
            print tsp_branch_bound(1,rc,0,len(locations))
            print 'file: ', sys.argv[2]
        if sys.argv[1] == '-c':
            print mutate_swap_two_cities()
            print 'file: ', sys.argv[2]
        if sys.argv[1] == '-t':
            print mutate_reverse_sub_tour()
            print 'file: ', sys.argv[2]
    # print locations

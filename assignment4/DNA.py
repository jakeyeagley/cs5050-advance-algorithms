#!/user/bin/python
import sys
import os
import time
from random import randint
from matplotlib import pyplot as plt

# this function returns the score for two DNA letters as follows
# +  A  C  G  T  -
# A  5 -1 -2 -1 -3
# C -1  5 -3 -2 -4
# G -2 -3  5 -2 -2
# T -1 -2 -2  5 -1
# - -3 -4 -2 -1  *
def score(F,S):
    if F == ' ':
        F = '-'
    if S == ' ':
        S = '-'
        
    if F == S and F != '-':
        return 5
    if F == 'A':
        if S == 'C' or S == 'T':
            return -1
        if S == 'G':
            return -2
        if S == '-':
            return -3
    if F == 'C':
        if S == 'A':
            return -1
        if S == 'G':
            return -3
        if S == 'T':
            return -2
        if S == '-':
            return -4
    if F == 'G':
        if S == 'A':
            return -2
        if S == 'C':
            return -3
        if S == 'T':
            return -2
        if S == '-':
            return -2
    if F == 'T':
        if S == 'A':
            return -1
        if S == 'C':
            return -2
        if S == 'G':
            return -2
        if S == '-':
            return -1
    if F == '-':
        if S == 'A':
            return -3
        if S == 'C':
            return -4
        if S == 'G':
            return -2
        if S == 'T':
            return -1
        
def DP(F,S,cache):
    A = len(F)
    B = len(S)

    # store base case scores in cache
    for j in range(0, B + 1):
        cache[0][j] = j
    for i in range(0, A + 1):
        cache[i][0] = i

    # look into cache and finding max score
    for i in range(1, A + 1):
        for j in range(1, B + 1):
            cache[i][j] = max(cache[i-1][j-1] + score(F[i-1],S[j-1]),cache[i-1][j] + score(F[i-1],'-'),cache[i][j-1] + score('-',S[j-1]))

def Traceback(A,B,cache):
    alignmentA = ''
    alignmentB = ''

    i = len(A)
    j = len(B)

    # assigning matched letter with score
    while (i > 0 or j > 0):
        if (i > 0 and j > 0 and cache[i][j] == cache[i-1][j-1] + score(A[i-1],B[j-1])):
            alignmentA = A[i-1] + alignmentA
            alignmentB = B[j-1] + alignmentB
            i = i - 1
            j = j - 1
        elif (i > 0 and cache[i][j] == cache[i-1][j] + score(A[i-1],'-')):
            alignmentA = A[i-1] + alignmentA
            alignmentB = "-" + alignmentB
            i = i - 1
        else:
            alignmentA = "-" + alignmentA
            alignmentB = B[j-1] + alignmentB
            j = j - 1

    return alignmentA, alignmentB

# shorter string needs to come first
def match(A,B,cache):
    DP(A,B,cache)
    return Traceback(A,B,cache)

# run the second study
def runSecond():
    fileStrands = []
    for root, dirs, files in os.walk('human\ matches', topdown=False):
        for name in files:
            fileStrands.append(os.path.join(root, name))

    # open files
    print('fileStands: ',fileStrands)
    for k in range(len(fileStrands) - 1,0,-1):
        for j in range(0,k - 1):
            with open(fileStrands[k]) as txt:
                A = ""
                for line in txt:
                    A = A + line
            with open(fileStrands[j]) as txt:
                B = ""
                for line in txt:
                    B = B + line

            #remove numbers from string
            A = ''.join([i for i in A if not i.isdigit()])
            B = ''.join([i for i in B if not i.isdigit()])
            
            # remove spaces from strings
            A = A.replace(" ","")
            B = B.replace(' ','')

            A = A.replace('\t','')
            A = A.replace('\n','')
            A = A.replace('\r','')
            B = B.replace('\t','')
            B = B.replace('\n','')
            B = B.replace('\r','')

            # initialize cache
            cache = []
            for i in range(0,len(A) + 1):
                new = []
                for w in range(0,len(B) + 1):
                    new.append(0)
                cache.append(new)

            # run algorithm
            alignments = []
            alignments = match(A.upper(),B.upper(),cache)

            # write to file
            with open('human_alignment_table.txt','a') as file:
                sys.stdout = file
                print(fileStrands[k], 'and', fileStrands[j])
                print(alignments[0])
                print(' ')
                print(alignments[1])
                print(' ')

# run the first study
def runFirst():
    # open files
    with open("input2.txt") as txt:
        A = ""
        for line in txt:
            A = A + line
    with open("input1.txt") as txt:
        B = ""
        for line in txt:
            B = B + line

    #remove numbers from string
    A = ''.join([i for i in A if not i.isdigit()])
    B = ''.join([i for i in B if not i.isdigit()])
    
    # remove spaces from strings
    A = A.replace(" ","")
    B = B.replace(' ','')

    A = A.replace('\t','')
    A = A.replace('\n','')
    A = A.replace('\r','')
    B = B.replace('\t','')
    B = B.replace('\n','')
    B = B.replace('\r','')

    # initialize cache
    cache = []
    for i in range(0,len(A) + 1):
        new = []
        for j in range(0,len(B) + 1):
            new.append(0)
        cache.append(new)

    # run algorithm         
    print("A: ",A.upper())
    print("B: ",B.upper())
    alignments = []
    alignments = match(A.upper(),B.upper(),cache)
    print (alignments)

    # write to file
    with open('first_study.txt','w') as file:
        sys.stdout = file
        # print out to file in vertical format
        for i in range(0,len(alignments[0])):
            print(alignments[0][i], ' ', alignments[1][i])

# run the third study
def runThird():
    fileStrands = []
    for root, dirs, files in os.walk('monkey\ matches', topdown=False):
        for name in files:
            fileStrands.append(os.path.join(root, name))

    print('fileStands: ',fileStrands)
    for k in range(len(fileStrands) - 1,0,-1):
        for j in range(0,k - 1):
            with open(fileStrands[k]) as txt:
                A = ""
                for line in txt:
                    A = A + line
            with open(fileStrands[j]) as txt:
                B = ""
                for line in txt:
                    B = B + line

            #remove numbers from string
            A = ''.join([i for i in A if not i.isdigit()])
            B = ''.join([i for i in B if not i.isdigit()])
            
            # remove spaces from strings
            A = A.replace(" ","")
            B = B.replace(' ','')

            A = A.replace('\t','')
            A = A.replace('\n','')
            A = A.replace('\r','')
            B = B.replace('\t','')
            B = B.replace('\n','')
            B = B.replace('\r','')

            # initialize cache
            cache = []
            for i in range(0,len(A) + 1):
                new = []
                for w in range(0,len(B) + 1):
                    new.append(0)
                cache.append(new)

            # run algorithm 
            alignments = []
            alignments = match(A.upper(),B.upper(),cache)

            # writing to file
            with open('monkey_alignment_table.txt','a') as file:
                sys.stdout = file
                print(fileStrands[k], 'and', fileStrands[j])
                print(alignments[0])
                print(' ')
                print(alignments[1])
                print(' ')

# run a simple test between two DNA strings
def runSimple():
    A = input('A: ')
    B = input('B: ')
    #remove numbers from string
    A = ''.join([i for i in A if not i.isdigit()])
    B = ''.join([i for i in B if not i.isdigit()])

    # remove spaces from strings
    A = A.replace(" ","")
    B = B.replace(' ','')
    A = A.replace('\t','')
    A = A.replace('\n','')
    A = A.replace('\r','')
    B = B.replace('\t','')
    B = B.replace('\n','')
    B = B.replace('\r','')

    cache = []
    for i in range(0,len(A) + 1):
        new = []
        for w in range(0,len(B) + 1):
            new.append(0)
        cache.append(new)
        
    alignments = []
    alignments = match(A.upper(),B.upper(),cache)
    print(alignments)

def runSize():
    letter = ['a','t','c','g']
    times = []
    lengths = []

    A = "atcgatcg"
    B = "atcgatcg"

    for j in range(0,1000):
        A = A + letter[randint(0,3)] + letter[randint(0,3)] + letter[randint(0,3)] + letter[randint(0,3)]
        B = B + letter[randint(0,3)] + letter[randint(0,3)] + letter[randint(0,3)] + letter[randint(0,3)]

        cache = []
        for i in range(0,len(A) + 1):
            new = []
            for w in range(0,len(B) + 1):
                new.append(0)
            cache.append(new)
            
        alignments = []
        start = 0
        start = time.time()
        
        alignments = match(A.upper(),B.upper(),cache)
        
        end = 0
        end = time.time()

        if j % 10 == 0:
            print('j: ', j , 'time: ', end-start)
        times.append(end - start)
        lengths.append(len(A))

    print('total time: ', sum(times))
    plt.plot(lengths,times)
    plt.xlabel('number of characters')
    plt.ylabel('time(ms)')
    plt.show()
if __name__ == '__main__':
    #runSimple()
    #runFirst()
    #runSecond()
    #runThird()
    runSize()

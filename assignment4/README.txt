DNA strings were shorten to the first ten lines listed on the different webpages. This was to speed up processing 
There are 5 functions (runFirst(), runSecond(), runThird(), runSimple() runSize()). 3 are for each study listed in the assignment (runFirst(), runSecond(), and runThird()), runSimple is to test simple strings, and runSize() is to test the maximum size for O(n^2)

Estimated max size for O(n^2) on my machine is ~20,000 characters. After ~20,000 characters is reached, my machine begins to freeze. 
